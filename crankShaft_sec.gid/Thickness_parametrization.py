from __future__ import print_function, absolute_import, division #makes KratosMultiphysics backward compatible with python 2.6 and 2.7

import KratosMultiphysics
from KratosMultiphysics.StructuralMechanicsApplication.structural_mechanics_analysis import StructuralMechanicsAnalysis
import json

"""
For user-scripting it is intended that a new class is derived
from StructuralMechanicsAnalysis to do modifications
"""
if __name__ == "__main__":

    # open the material file
    with open("StructuralMaterials.json", "r") as read_file:
        StructuralMaterials = json.load(read_file)
    # modify the thickness of the model parts
    StructuralMaterials["properties"][0]["Material"]["Variables"]["THICKNESS"] = 20.0
    StructuralMaterials["properties"][1]["Material"]["Variables"]["THICKNESS"] = 20.0
    StructuralMaterials["properties"][2]["Material"]["Variables"]["THICKNESS"] = 20.0
    StructuralMaterials["properties"][3]["Material"]["Variables"]["THICKNESS"] = 20.0
    StructuralMaterials["properties"][4]["Material"]["Variables"]["THICKNESS"] = 20.0
    # write the material file
    with open("StructuralMaterials.json", "w") as write_file:
        json.dump(StructuralMaterials, write_file)

    with open("ProjectParameters.json",'r') as parameter_file:
        parameters = KratosMultiphysics.Parameters(parameter_file.read())

    model = KratosMultiphysics.Model()
    simulation = StructuralMechanicsAnalysis(model,parameters)
    simulation.Run()
