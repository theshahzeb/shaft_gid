from __future__ import absolute_import, division, print_function
import KratosMultiphysics as km
import KratosMultiphysics.StructuralMechanicsApplication as sma
import numpy as np
import matplotlib as mp
import matplotlib.pyplot as plt
# #to save tex files, very smart
from tikzplotlib import save as tikz_save
#import tikzplotlib
from mpl_toolkits.mplot3d import Axes3D
#import scipy
#from scipy import signal

#do we need all this? lets see
#Ortho = 1
#ParameterStudy1D = 0
#ParameterStudy2D = 0
opt = 0
Analysis = 1

t = np.linspace(18.0, 23.0, num=10) #thickness
#sections = [0, 1, 2, 3, 4]


def Evaluation(Thick):
    from KratosMultiphysics.StructuralMechanicsApplication.structural_mechanics_analysis import StructuralMechanicsAnalysis
    class StructuralMechanicsAnalysis(StructuralMechanicsAnalysis):
        def ModifyInitialProperties(self):
            mpName = self.project_parameters["solver_settings"]["model_part_name"].GetString()
            mp = self.model[mpName]
            for i, elem in enumerate(mp.Elements):
                for i in range(len(t)):
                    tParametrized = t[i]

                elem.SetValue(km.THICKNESS, tParametrized)


                #mp.GetProperties()[1].SetValue(km.CONSTITUTIVE_LAW, cl)

        def RunSolutionLoop(self):
            while self.time < self.end_time:
                self.time = self._GetSolver().AdvanceInTime(self.time)
                self.InitializeSolutionStep()
                self._GetSolver().Predict()
                self._GetSolver().SolveSolutionStep()
                self.FinalizeSolutionStep()
                self.OutputSolutionStep()

        def Finalize(self):
            super(StructuralMechanicsAnalysis, self).Finalize()
            mpName = self.project_parameters["solver_settings"]["model_part_name"].GetString()
            mp = self.model[mpName]
            mass_process = sma.TotalStructuralMassProcess(mp)
            mass_process.Execute()
            self.mass = mp.ProcessInfo[km.NODAL_MASS]
            self.disp = [[]]*len(mp.Nodes)
            self.rot = [[]]*len(mp.Nodes)
            for ii in range(len(mp.Nodes)):
                self.disp[ii] = mp.Nodes[ii+1].GetSolutionStepValue(km.DISPLACEMENT)
                self.rot[ii] = mp.Nodes[ii+1].GetSolutionStepValue(km.ROTATION)
            self.tw = [[]]*len(mp.Elements)
            self.stressM = [[]]*len(mp.Elements)
            self.MatAng = [[]]*len(mp.Elements)
            for ii in range(len(mp.Elements)):
                self.stressM[ii] = mp.Elements[ii+1].CalculateOnIntegrationPoints(sma.VON_MISES_STRESS,
                                                                                  mp.ProcessInfo)[0]


    with open("ProjectParameters.json", 'r') as parameter_file:
        parameters = km.Parameters(parameter_file.read())
    model = km.Model()
    simulation = StructuralMechanicsAnalysis(model, parameters)
    simulation.Initialize()
    simulation.ModifyInitialProperties()
    simulation.RunSolutionLoop()
    simulation.Finalize()
    return(simulation.mass, simulation)

#is this optimization?
#def SysEq(x, gc):
#    mass, sim = Evaluation(x)
#    f = -min(sim.tw)*100
#    g = []
#    return(f, g)


#if ParameterStudy1D == 1:
#    import time
#    start = time.time()
#    neval = 101
#    Theta = np.linspace(-90, 90, neval)
#    tw = np.zeros(len(Theta))
#    stressM = np.zeros(len(Theta))
#    T = [[]]*len(Theta)
#    dispX = np.zeros(len(Theta))
#    dispY = np.zeros(len(Theta))
#    dispZ = np.zeros(len(Theta))
#    dispFiber = [[]]*len(Theta)
#    plt.figure(1, figsize=(12, 10))
#    for ii in range(len(Theta)):
#        start = time.time()
#
#        mass, sim = Evaluation(Theta[ii])
#        redisp = np.reshape(sim.disp, (len(sim.disp), 3))
#
#        tw[ii] = min(sim.tw)
#        stressM[ii] = sim.stressM[210]
#        dispX[ii] = max(redisp[:, 0])
#        dispY[ii] = redisp[:, 1][1]
#        dispZ[ii] = max(redisp[:, 2])
#        end = time.time()
#        print ("Estimated Time [min]= ", (neval-ii)*(end-start)/60)

#plots
    plt.subplot(3, 1, 1)
    plt.plot(Thick, mass, 'm')
    plt.xlabel('thickness [mm]')
    plt.ylabel('nodal mass')
    #plt.axis([-90, 90, 0, 200])
    plt.grid(True)

    plt.subplot(3, 1, 2)
    plt.plot(Thick, stressM, 'sigma-')
    plt.xlabel('thickness [mm]')
    plt.ylabel('Equivalent stress von Mises [MPa]')
#    plt.axis([-90, 90, 40, 60])
    plt.grid(True)

    plt.subplot(3, 1, 3)
    plt.plot(Thick, disp, 'go-')
    plt.ylabel('Displacement [mm]')
    plt.xlabel('thickness [mm]')
    #plt.axis([-90, 90, -0.1, 0.1])
    plt.grid(True)

    plt.grid(True)
    tikz_save("Shell_tension_Plot.tex", figureheight='\\figureheight', figurewidth='\\figurewidth')

    plt.show()
    end = time.time()
    print("Numerical evaluation time [sec.] = ", end - start)


if Analysis:
    mass, sim = Evaluation([0])
    print(max(sim.stressM))
    disp = np.reshape(sim.disp, (len(sim.disp), 3))
    print(max(disp[:, 1]))
    print(sim.mass)


#mass, sim = Evaluation(0)

#print(sim.MatAng)
#print(sim.MatAxis1)
#print(sim.MatAxis2)
